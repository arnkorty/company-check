class CompanyInfo < ActiveRecord::Base
  validates :license_no,:license_file,:street_address1,:city,:state,:zip_code,
  :country,:tel1,:tel2,:fax1,:fax2,
  :date_of_incorporation,:brand_ownership,:product_category,
  :brand_list,:safes_achieved_in_2014,:contact_person,:language_en_level,
  :language_zh_level,:mother_tongue,:office_tel,:cell_phone,:email,:bank_account,
  :beneficiary,:swift_no,:bank_name, presence: true

  validates :tel_ext, :fax_ext, length: { maximum: 5 }
  validates :email, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}, uniqueness: true

  STATUSES = ["Review", 'Inform supplier', 'Payment Received', 'Contract signed', 'Successful']

  mount_uploader :license_file, LicenseFileUploader
  mount_uploader :agency_terms_file, AgencyTermUploader

  belongs_to :company

  delegate :name, to: :company, prefix: true, allow_nil: true

  def name
    "#{company_name}(#{email})"
  end

  def check_status
    ['Pending', *(['Payment&Contract signing'] * 3), 'Successful'][STATUSES.index(status).to_i]
    #self.checked ? "finished" : "pendding"
  end

  def successful?
    status == STATUSES[-1]
  end

  def pending?
    check_status == 'Pending'
  end
end
