# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150726103958) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "email",           limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "company_infos", force: :cascade do |t|
    t.integer  "company_id",                              limit: 4
    t.string   "license_no",                              limit: 255
    t.string   "license_file",                            limit: 255
    t.string   "street_address1",                         limit: 255
    t.string   "street_address2",                         limit: 255
    t.string   "city",                                    limit: 255
    t.string   "zip_code",                                limit: 255
    t.string   "state",                                   limit: 255
    t.string   "country",                                 limit: 255
    t.string   "tel1",                                    limit: 255
    t.string   "tel2",                                    limit: 255
    t.string   "tel_ext",                                 limit: 255
    t.string   "fax1",                                    limit: 255
    t.string   "fax2",                                    limit: 255
    t.string   "fax_ext",                                 limit: 255
    t.string   "company_website",                         limit: 255
    t.date     "date_of_incorporation"
    t.string   "brand_ownership",                         limit: 255
    t.text     "product_category",                        limit: 65535
    t.string   "brand_list",                              limit: 255
    t.integer  "safes_achieved_in_2014",                  limit: 4
    t.string   "address_of_flagship_store",               limit: 255
    t.string   "contact_person",                          limit: 255
    t.string   "mother_tongue",                           limit: 255
    t.string   "office_tel",                              limit: 255
    t.string   "cell_phone",                              limit: 255
    t.string   "email",                                   limit: 255
    t.string   "wechat_account",                          limit: 255
    t.string   "skype_account_skype",                     limit: 255
    t.string   "qq",                                      limit: 255
    t.boolean  "china_agent_company"
    t.string   "china_agent_company_name",                limit: 255
    t.boolean  "wish_find_agent_in_china"
    t.string   "agency_terms_file",                       limit: 255
    t.string   "online_shop_website",                     limit: 255
    t.string   "sales_channel",                           limit: 255
    t.integer  "turnover_of_online_safes_in_2014",        limit: 4
    t.integer  "number_of_operation_team_members",        limit: 4
    t.integer  "number_of_customer_service_team_members", limit: 4
    t.integer  "number_of_technical_team_members",        limit: 4
    t.integer  "warehouse_size",                          limit: 4
    t.string   "bank_account",                            limit: 255
    t.string   "bank_name",                               limit: 255
    t.string   "beneficiary",                             limit: 255
    t.string   "swift_no",                                limit: 255
    t.string   "payment_terms_with_ecat",                 limit: 255
    t.string   "product_inventory_location",              limit: 255
    t.string   "logistics_mode",                          limit: 255
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.string   "language_en_level",                       limit: 255
    t.string   "language_zh_level",                       limit: 255
    t.text     "company_profile",                         limit: 65535
    t.string   "status",                                  limit: 30,    default: "Review"
  end

  add_index "company_infos", ["company_id"], name: "index_company_infos_on_company_id", using: :btree

  create_table "info_tempfiles", force: :cascade do |t|
    t.string   "license_file",      limit: 255
    t.string   "agency_terms_file", limit: 255
    t.string   "stringß",           limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

end
