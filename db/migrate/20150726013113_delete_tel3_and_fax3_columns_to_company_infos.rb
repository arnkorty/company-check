class DeleteTel3AndFax3ColumnsToCompanyInfos < ActiveRecord::Migration
  def change
    remove_column :company_infos, :tel3, :string
    remove_column :company_infos, :fax3, :string
  end
end
