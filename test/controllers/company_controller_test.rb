require 'test_helper'

class CompanyControllerTest < ActionController::TestCase
  test "should get register" do
    get :register
    assert_response :success
  end

  test "should get info" do
    get :info
    assert_response :success
  end

end
