ActiveAdmin.register CompanyInfo do
  actions :all, except: [:edit, :create, :destroy, :new]
  index do
    id_column
    column :email
    column :company_name do |info|
      info.company_name
    end
    [:status, :license_no, :street_address1, :city, :state, :country, :zipcode, :company_website].each do |col|
      column col
    end
    actions
  end

  csv do
    column :id
    column :status
    column(:company_name) { |info| info.company_name}
    column :license_no
    column(:license_file){|info|
      info.license_file.url ? URI.join(request.base_url,info.license_file.url) : nil
    }
    column :company_profile
    column :street_address1
    column :street_address2
    column :city
    column :state
    column("ZIP / Postal code"){|info| info.zip_code}
    column :country
    column("Tel"){|info|
      [info.tel1, info.tel2, info.tel_ext].compact.join('-')
    }
    column("Fax"){|info|
      [info.fax1, info.fax2, info.fax_ext].compact.join('-')
    }
    column :company_website
    column :date_of_incorporation
    column :brand_ownership
    column :product_category
    column :brand_list
    column("Sales achieved in 2014 (USD)"){|info| info.safes_achieved_in_2014}
    column :address_of_flagship_store
    column :contact_person
    column :mother_tongue
    column :language_en_level
    column :language_zh_level
    column :office_tel
    column :wechat_account
    column :cell_phone
    column('Skype account'){|info| info.skype_account_skype}
    column :email
    column :qq
    column :china_agent_company
    column :wish_find_agent_in_china
    column(:agency_terms_file){ |info|
        info.agency_terms_file.url ? URI.join(request.base_url,info.agency_terms_file.url) : nil
    }
    column :online_shop_website
    column :sales_channel
    column("Turnover of online sales in 2014 (USD)"){|info| info.turnover_of_online_safes_in_2014}
    column :number_of_operation_team_members
    column :number_of_customer_service_team_members
    column :number_of_technical_team_members
    column :warehouse_size
    column :bank_account
    column :bank_name
    column :beneficiary
    column :swift_no
    column :payment_terms_with_ecat
    column :product_inventory_location
    column :logistics_mode
    column :created_at
    column :updated_at
  end

  show do |info|
    div(class: 'container') do
      #render "admin/company_infos/status_form", info: info
      render "admin/company_infos/info", info: info
    end
  end

  member_action :fresh, method: [:post, :patch, :get] do
    return redirect_to admin_company_info_path(resource) if request.get?
    if request.xhr? && params[:status]
      old_index = CompanyInfo::STATUSES.index resource.status
      index = CompanyInfo::STATUSES.index params[:status]
      if index == (old_index + 1) && resource.update_attributes(status: params[:status])
        render json: {index: index}
      else
        render json: {index: old_index}, status: 409
      end
    end
    @page_title = resource.name
    attrs = params.require(:company_info).permit(CompanyInfo.column_names) if params[:company_info]
    if attrs
      attrs.delete :license_file if attrs[:license_file].blank?
      attrs.delete :agency_terms_file if attrs[:agency_terms_file].blank?
      resource.update_attributes attrs
    end
  end

  #member_action :check do
  #resource.update_attributes checked: true
  #redirect_to admin_company_info_path(resource)
  #end

  #member_action :uncheck do
  #resource.update_attributes checked: false
  #redirect_to admin_company_info_path(resource)
  #end
end
