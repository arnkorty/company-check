#= require active_admin/base
ActiveAdmin.status_check = (ts)->
  self = $(ts)
  $.post window.location.href + '/fresh', {status: self.val()},  (data)->
    self.attr('disabled', true)
    self.addClass('checked')
    label = $('.status-check-boxes label[data-index='+data.index + ']').removeClass('unchecked').addClass('checked')
    target = $('.unactive[data-index='+ (data.index + 1) + ']')
    $("#check-status-name").html(self.val())
    if target.length == 1
      target.removeClass('unactive')
      target.attr('disabled', false)
      target.attr('onclick', 'ActiveAdmin.status_check(this);')
      alert('Status changed successful')
