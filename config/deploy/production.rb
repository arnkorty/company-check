set :domain, 'supplier.ecat.mobi'
set :deploy_to, '/home/ecat/www/check'
set :repository, 'git@bitbucket.org:arnkorty/company-check.git'
set :branch, 'master'
set :user, 'ecat'
set :forward_agent, true
