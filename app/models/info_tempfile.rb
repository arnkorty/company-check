class InfoTempfile < ActiveRecord::Base
  mount_uploader :license_file, LicenseFileUploader
  mount_uploader :agency_terms_file, AgencyTermUploader
end
