class CreateInfoTempfiles < ActiveRecord::Migration
  def change
    create_table :info_tempfiles do |t|
      t.string :license_file
      t.string :agency_terms_file
      t.string :stringß

      t.timestamps null: false
    end
  end
end
