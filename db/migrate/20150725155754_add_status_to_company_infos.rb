class AddStatusToCompanyInfos < ActiveRecord::Migration
  def up
    add_column :company_infos, :status, :string, limit: 30, default: 'Review'
    CompanyInfo.all.each do |cinfo|
      if cinfo.checked
        cinfo.update_columns status: 'Successful'
      end
    end
    remove_column :company_infos, :checked
  end
  def down
    remove_column :company_infos, :status
    add_column :company_infos, :checked, :boolean
  end
end
