class SessionsController < ApplicationController
  def new
    @company = Company.new
  end

  def create
    @company = Company.find_by_email(params[:email])
    if @company && @company.authenticate(params[:password])
      session[:company_id] = @company.id
      redirect_to company_info_path
    else
      @company ||= Company.new
      render :new
    end
  end

  def logout
    reset_session
    redirect_to root_path
  end
end
