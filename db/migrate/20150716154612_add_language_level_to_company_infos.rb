class AddLanguageLevelToCompanyInfos < ActiveRecord::Migration
  def change
    add_column :company_infos, :language_en_level, :string
    add_column :company_infos, :language_zh_level, :string
    remove_column :company_infos, :language_name, :string
    remove_column :company_infos, :language_level, :string
  end
end
