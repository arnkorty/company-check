# encoding: utf-8
class CompanyController < ApplicationController
  before_action :request_login!, only: :info
  def register
    @company ||= Company.new
    if params[:company] && params[:agreecheck]
      @company = Company.new params[:company].slice(:name,:email, :password, :password_confirmation).to_hash
      if @company.save
        session[:company_id] = @company.id
        redirect_to company_info_path
      end
    end
  end

  def info
    @info = current_company.info || current_company.build_info
    attrs = info_params
    if attrs && (@info.new_record? || @info.license_file.blank? || @info.agency_terms_file.blank)
      if session[:temp_id]
        @temp = InfoTempfile.find session[:temp_id]
      else
        @temp = InfoTempfile.new
      end
      @temp.license_file = attrs[:license_file] || @temp.license_file
      @temp.agency_terms_file = attrs[:agency_terms_file] || @temp.agency_terms_file
      @temp.save
      session[:temp_id] = @temp.id
      @info.license_file = @temp.license_file
      @info.agency_terms_file = @temp.agency_terms_file
    end
    if attrs && @info.pending?
      attrs.delete :license_file if attrs[:license_file].blank?
      attrs.delete :agency_terms_file if attrs[:agency_terms_file].blank?
      @info.update_attributes attrs
    end
  end

  def info_params
    params.require(:company_info).permit(CompanyInfo.column_names.select{|s| s != 'status'}) if params[:company_info]
  end
end
