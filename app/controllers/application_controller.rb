class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_company
    @current_company ||= Company.find_by_id(session[:company_id])
  end
  def request_login!
    unless current_company
      return redirect_to login_path
    end
  end
end
