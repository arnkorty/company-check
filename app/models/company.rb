class Company < ActiveRecord::Base
  has_secure_password
  validates :name, presence: true
  validates :email, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}, uniqueness: true
  has_one :info, class_name: CompanyInfo
end
