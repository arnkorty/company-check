class AddCompanyProfileToCompanyInfos < ActiveRecord::Migration
  def change
    add_column :company_infos, :company_profile, :text
  end
end
