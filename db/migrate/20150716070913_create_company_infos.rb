class CreateCompanyInfos < ActiveRecord::Migration
  def change
    create_table :company_infos do |t|
      t.integer :company_id, index: true
      t.string :license_no
      t.string :license_file
      t.string :street_address1
      t.string :street_address2
      t.string :city
      t.string :zip_code
      t.string :state
      t.string :country
      t.string :tel1
      t.string :tel2
      t.string :tel3
      t.string :tel_ext
      t.string :fax1
      t.string :fax2
      t.string :fax3
      t.string :fax_ext
      t.string :company_website
      t.date :date_of_incorporation
      t.string :brand_ownership
      t.text :product_category
      t.string :brand_list
      t.integer :safes_achieved_in_2014
      t.string :address_of_flagship_store
      t.string :contact_person
      t.string :language_name
      t.string :language_level
      t.string :mother_tongue
      t.string :office_tel
      t.string :cell_phone
      t.string :email
      t.string :wechat_account
      t.string :skype_account_skype
      t.string :qq
      t.boolean :china_agent_company
      t.string :china_agent_company_name
      t.boolean :wish_find_agent_in_china
      t.string :agency_terms_file
      t.string :online_shop_website
      t.string :sales_channel
      t.integer :turnover_of_online_safes_in_2014
      t.integer :number_of_operation_team_members
      t.integer :number_of_customer_service_team_members
      t.integer :number_of_technical_team_members
      t.integer :warehouse_size
      t.string :bank_account
      t.string :bank_name
      t.string :beneficiary
      t.string :swift_no
      t.string :payment_terms_with_ecat
      t.string :product_inventory_location
      t.string :logistics_mode
      t.boolean :checked

      t.timestamps null: false
    end
  end
end
